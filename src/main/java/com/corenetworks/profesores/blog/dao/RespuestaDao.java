package com.corenetworks.profesores.blog.dao;

import java.util.List;

import javax.persistence.EntityManager;
//import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.corenetworks.profesores.blog.model.Respuesta;


@Repository
@Transactional
public class RespuestaDao {
	
    @PersistenceContext
	private EntityManager entityManager;
    /*
     * Almacena la respuesta en la base de datos
     */
    public void create(Respuesta respuesta) {
    	entityManager.persist(respuesta);
    	return;
    }
    
    @SuppressWarnings("unchecked")
	public List<Respuesta> getAll(){
    	return entityManager
    			.createQuery("select r from respuesta r")
    			.getResultList();
    }
    
    
 	/**
	 * Devuelve un post en base a su Id
	 */
	public Respuesta getById(long id) {
		return entityManager.find(Respuesta.class, id);
	}

    
    
    
}
