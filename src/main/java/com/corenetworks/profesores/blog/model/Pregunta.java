package com.corenetworks.profesores.blog.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

//import org.hibernate.annotations.ColumnTransformer;
import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name="pregunta")
public class Pregunta {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@Column
	private String titulo;
	
	
	@Column
	@Lob
	private String contenido;
	
	@Column
	@CreationTimestamp
	private Date fecha;
		
	@ManyToOne
	private Profesor profesor;
    
	@OneToMany(mappedBy="pregunta")
	private List<Respuesta> respuestas = new ArrayList<>();
	
	public Pregunta() {
		super();
	}

	public Pregunta(String url, String titulo, String contenido, Profesor profesor) {
		super();
		this.titulo = titulo;
		this.contenido = contenido;
		this.profesor = profesor;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getContenido() {
		return contenido;
	}

	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Profesor getProfesor() {
		return profesor;
	}

	public void setProfesor(Profesor profesor) {
		this.profesor = profesor;
	}

	public List<Respuesta> getRespuesta() {
		return respuestas;
	}

	public void setRespuesta(List<Respuesta> respuestas) {
		this.respuestas = respuestas;
	}
	
	
    
}
