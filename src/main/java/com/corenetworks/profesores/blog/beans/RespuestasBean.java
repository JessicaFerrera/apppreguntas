package com.corenetworks.profesores.blog.beans;

public class RespuestasBean {
	private String contenido;
	private long pregunta_id;

	public RespuestasBean() {
		super();
	}

	public String getContenido() {
		return contenido;
	}

	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

	public long getPregunta_id() {
		return pregunta_id;
	}

	public void setPregunta_id(long pregunta_id) {
		this.pregunta_id = pregunta_id;
	}

}
