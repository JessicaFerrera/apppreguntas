package com.corenetworks.profesores.blog.controller;


import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.corenetworks.profesores.blog.beans.RespuestasBean;
import com.corenetworks.profesores.blog.beans.PreguntasBean;
//import com.corenetworks.profesores.blog.beans.RegisterBean;
import com.corenetworks.profesores.blog.dao.PreguntaDao;
//import com.corenetworks.profesores.blog.dao.ProfesorDao;
import com.corenetworks.profesores.blog.dao.RespuestaDao;
import com.corenetworks.profesores.blog.model.Pregunta;
import com.corenetworks.profesores.blog.model.Profesor;
import com.corenetworks.profesores.blog.model.Respuesta;



@Controller
public class PreguntasController {
	@Autowired
	private PreguntaDao preguntaDao;
	
	@Autowired
	private RespuestaDao respuestaDao;
	
	@Autowired
	private HttpSession httpSession;

	@GetMapping(value = "/submit")
	public String showForm(Model modelo) {
		modelo.addAttribute("post", new PreguntasBean());
		return "submit";
	}
	
	@PostMapping(value = "/submit/newpregunta")
	public String submit(@ModelAttribute("pregunta") PreguntasBean preguntasBean , Model model) {
		//userDao.create(new User(r.getNombre(), r.getEmail(), r.getCiudad(), r.getPassword()));
         // crear una pregunta
		 // Obtener el profesor desde la sesión
		 // Asignar el profesor a la pregunta
		 // Hacer persistente la pregunta
		Pregunta pregunta = new Pregunta();
		pregunta.setTitulo(preguntasBean.getTitulo());
		pregunta.setContenido(preguntasBean.getContenido());
		
		
		Profesor profesor = (Profesor) httpSession.getAttribute("userLoggedIn");
		pregunta.setProfesor(profesor);
		preguntaDao.create(pregunta);
		profesor.getPreguntas().add(pregunta);
		
		return "redirect:/";
	}	
	
	@GetMapping(value = "/post/{id}")
	public String detail(@PathVariable("id")  long id,   Model modelo) {
		//modelo.addAttribute("post", new PostBean());
		// Comprobar si el Post existe.
		
		Pregunta result = null;
		if ((result = preguntaDao.getById(id)) != null) {
			modelo.addAttribute("post", result);
			modelo.addAttribute("respuestaForm", new RespuestasBean());
			return "preguntadetail";			
		} else
			return "redirect:/";
	}
	
	@PostMapping(value = "/submit/newrespuesta")
	public String submitComment(@ModelAttribute("respuestaForm") RespuestasBean respuestasBean , Model model) {
		Profesor profesor = (Profesor) httpSession.getAttribute("userLoggedIn");
		
		Respuesta respuesta = new Respuesta();
		respuesta.setProfesor(profesor);
		
		Pregunta pregunta = preguntaDao.getById(respuestasBean.getPregunta_id());
		respuesta.setPregunta(pregunta);
		respuesta.setContenido(respuestasBean.getContenido());
		respuestaDao.create(respuesta);
		pregunta.getRespuesta().add(respuesta);
		profesor.getRespuestas().add(respuesta);
		
		
		return "redirect:/pregunta/"+ respuestasBean.getPregunta_id();
	}	
	
	
}
