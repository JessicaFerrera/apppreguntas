package com.corenetworks.profesores.blog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.corenetworks.profesores.blog.beans.RegisterBean;
import com.corenetworks.profesores.blog.dao.ProfesorDao;
import com.corenetworks.profesores.blog.model.Profesor;

@Controller
public class RegisterController {
	@Autowired
	private ProfesorDao profesorDao;

	@GetMapping(value = "/signup")
	public String showForm(Model model) {
		model.addAttribute("profesorRegister", new RegisterBean());
		return "register";
	}

	@PostMapping(value = "/register")
	public String submit(@ModelAttribute("profesorRegister") RegisterBean r, Model model) {
		profesorDao.create(new Profesor(r.getNombre(), r.getEmail(), r.getCiudad(), r.getPassword()));

		return "redirect:/autores";
	}
}
